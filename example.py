from asperitas import *

handler = AsperitasResource()

@handler.create
def creation(event, context):
    # Create the resource
    return {
        "PhysicalResourceId": "i-1234566"
    }

@handler.update
def update(event, context):
    # Update the resource
    return {
        "PhysicalResourceId": "i-1234566"
    }

@handler.delete
def deletion(event, context):
    # Delete the resource
    pass