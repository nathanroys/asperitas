import logging
import json
import requests

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class ResponseBuilder:

    def __init__(self):
        self._response : dict = {}

    def success(self):
        self._response['Status'] = 'SUCCESS'
        return self

    def failed(self, reason : str):
        self._response['Status'] = 'FAILED'
        self._response['Reason'] = reason
        return self

    def physical_resource_id(self, id : str):
        self._response['PhysicalResourceId'] = id
        return self

    def stack_id(self, id : str):
        self._response['StackId'] = id
        return self

    def request_id(self, id : str):
        self._response['RequestId'] = id
        return self

    def logical_resource_id(self, id : str):
        self._response['LogicalResourceId'] = id
        return self

    def data(self, data_ : str):
        self._response.update(data_)
        return self

    def to_json(self) -> str:
        if self._valid_response():
            return json.dumps(self._response)

    def _valid_response(self):
        if 'Status' not in self._response:
            raise ValueError("Response must include a status")
        elif self._response['Status'] == 'FAILED' and 'Reason' not in self._response:
            raise ValueError("When status is 'FAILED', 'Reason' must be set")
        elif 'StackId' not in self._response:
            raise ValueError("'StackId' is required")
        elif 'RequestId' not in self._response:
            raise ValueError("'RequestId' is required")
        elif 'LogicalResourceId' not in self._response:
            raise ValueError("'LogicalResourceId' is required")
        return True


class AsperitasResource:

    def __init__(self):
        self._handlers : dict = {}

    def send_response(self, response : str, to_url : str):
        try:
            request = requests.put(
                to_url,
                data=response,
                headers={'Content-Type':'', 'Content-Length':f"{len(response)}"}
            )
        except Exception as e:
            logging.error(e, exc_info=True)

    def __call__(self, event : str, context : str):
        logger.info(f"handling request, event data {str(event)}")

        try:
            response_builder = ResponseBuilder()

            request_type = event['RequestType']
            response_url = event['ResponseURL']
            stack_id = event['StackId']
            request_id = event['RequestId']
            resource_type = event['ResourceType']
            logical_resource_id = event['LogicalResourceId']

            if 'PhysicalResourceId' in event:
                physical_resource_id = event['PhysicalResourceId']
                response_builder.physical_resource_id(physical_resource_id)

            # set response variables we know we have
            response_builder.stack_id(stack_id)
            response_builder.logical_resource_id(logical_resource_id)
            response_builder.request_id(request_id)

            user_return_data = self._handlers[request_type](event, context)
            if user_return_data is not None:
                response_builder.data(user_return_data)

            if (request_type == 'Create' or request_type == 'Update') and 'PhysicalResourceId' not in user_return_data:
                logger.error("when the request type is 'Create' or 'Update' a PhysicalResourceId must be set")
                self.send_response(
                    response_builder
                        .failed("when the request type is 'Create' or 'Update' a PhysicalResourceId must be set")
                        .to_json(),
                    response_url
                )
            else:
                response = response_builder.success().to_json()
                logger.info(f"responding with {response}")
                self.send_response(
                    response,
                    response_url
                )
        except Exception as e:
            logging.error(e, exc_info=True)
            self.send_response(
                response_builder.failed(f"An exeception occured whilst processing the request. Error: {str(e)}")
                .to_json(),
                response_url
            )


    def create(self, handler):
        self._handlers['Create'] = handler

    def update(self, handler):
        self._handlers['Update'] = handler

    def delete(self, handler):
        self._handlers['Delete'] = handler