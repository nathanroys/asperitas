# Asperitas

## Introduction
Asperitas is an AWS CloudFormation custom resource helper for Python. Asperitas uses Python 
decorators to define creation, deletion and update functions.

Asperitas is written for Python 3.

## Example
* Note: Both 'Create' and 'Update' events need to return a 'PhysicalResourceId'.
* In the following example your Lambda function entry point should be set to {file}.handler
* Raise an exception for to send back a 'FAILED' response

```python
from asperitas import *

handler = AsperitasResource()

@handler.create
def creation(event, context):
    # Create the resource
    return {
        "PhysicalResourceId": "i-1234566"
    }

@handler.update
def update(event, context):
    # Update the resource
    return {
        "PhysicalResourceId": "i-1234566"
    }

@handler.delete
def deletion(event, context):
    # Delete the resource
    pass
```

## Thanks
[https://github.com/requests/requests](https://github.com/requests/requests)

[https://github.com/ryansb/cfn-wrapper-python](https://github.com/ryansb/cfn-wrapper-python)